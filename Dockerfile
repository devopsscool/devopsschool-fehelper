FROM gradle:6.7.1-jdk8
RUN apt update && apt install npm -y && npm install --global yarn@1.22.11
CMD ["/bin/sh"]
